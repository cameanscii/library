package pl.sda.bib;

public class Biblioteka {
    private Egzemplarz[] zbior;

    public Biblioteka(Egzemplarz[] zbior) {
        this.zbior = zbior;
    }

    public Egzemplarz[] szukajPoTytule(String tytul){
        Egzemplarz[] znalezione=new Egzemplarz[10];
        int iloscZnalezionych=0;
        for (Egzemplarz egzemplarz:zbior) {
            if (egzemplarz.pobierzTytul().contains(tytul)){
                znalezione[iloscZnalezionych]=egzemplarz;
                iloscZnalezionych++;
            }
        }
        return przytnij(znalezione);
    }

    public Egzemplarz[] szukajPoAutorze(String autor1){
        Egzemplarz[] znalezione=new Egzemplarz[10];
        int iloscZnalezionych=0;
        for (Egzemplarz egzemplarz:zbior) {
            for(Autor autor:egzemplarz.autorzy){
            if (autor.toString().contains(autor1)){
                znalezione[iloscZnalezionych]=egzemplarz;
                iloscZnalezionych++;
            }
            }
        }
        return przytnij(znalezione);
    }

    public Egzemplarz[] szukajPoAutorzeITytule(String autortytul){
        Egzemplarz[] znalezione=new Egzemplarz[10];
        int iloscZnalezionych=0;
        for (Egzemplarz egzemplarz:zbior) {
            if (egzemplarz.toString().contains(autortytul)){
                znalezione[iloscZnalezionych]=egzemplarz;
                iloscZnalezionych++;
            }
        }
        return przytnij(znalezione);
    }







    private Egzemplarz[] przytnij(Egzemplarz[] pozycje) {
        int niePuste=0;
        for (Egzemplarz egzemplarz:pozycje){
            if (egzemplarz!=null){
                niePuste++;
            }
        }
        Egzemplarz[] przycieta = new Egzemplarz[niePuste];
        for (int i = 0; i <przycieta.length ; i++) {
            przycieta[i]=pozycje[i];
        }
        return przycieta;
    }
}
